%define SYS_WRITE 1
%define SYS_EXIT 60
%define stdin 0
%define stdout 1

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count_length:
    	cmp byte [rdi+rax], 0
    	je .end
    	inc rax 
    	jmp .count_length
    .end:
    	ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov r9, rdi
	call string_length
	mov rdx, rax
	mov rax, SYS_WRITE
	mov rsi, r9
	mov rdi, stdout
	syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rdi, stdout
    mov rax, SYS_WRITE
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, 10
    push 0x0
    mov rax, rdi
    .div_number:
        xor rdx, rdx
        div r8
        add rdx, '0'
        push rdx
        cmp rax, 0
        je .print
        jmp .div_number
    .print:
        pop r8
        cmp r8, 0x0
        je .end
        mov rdi, r8
        call print_char
        jmp .print
    .end:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jl .negative
	jmp print_uint
.negative:
    mov r10, rdi
    mov rdi, '-'
    call print_char
    neg r10
    mov rdi, r10
	jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	.compare:
		mov r10b, byte[rdi]
        mov r11b, byte[rsi]
        cmp r10b, r11b
		jne .not_equals
    	test r10b, r10b
    	jz .equals
		inc rdi
		inc rsi
		jmp .compare
	.equals:
		mov rax, 1
		ret
	.not_equals:
		mov rax, 0
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push 0x0
    mov rdx, 1
    mov rdi, stdin
    mov rsi, rsp
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	mov r10, rdi
    xor r9, r9
	mov r11, rsi
	xor rax, rax
    .start_reading:
        push r10
        push r11
        call read_char
        pop r11
        pop r10
        cmp rax, rax
        je .check_spaces
    .check_spaces:
        cmp rax, 0x20
        je .start_reading
        cmp rax, 0x9
        je .start_reading
        cmp rax, '\n'
        je .start_reading
        jmp .read
    .read:
		cmp r9, r11
		je .fail
		cmp rax, 0x20
		je .add_null
		cmp rax, 0x9
		je .add_null
		cmp rax, 0xA
		je .add_null
		cmp rax, 0
		je .add_null
		mov [r10 + r9], rax
		inc r9
		call read_char
		jmp .read
    .add_null:
    	xor rax, rax
    	mov [r10 + r9], rax
    	jmp .end
    .fail:
        xor rax, rax
        ret
    .end:
     	mov rax, r10
     	mov rdx, r9
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r10, rdi
    xor rax, rax
    xor r9, r9
    xor r11, r11
    .parse:
        mov  r9b, byte[r10 + r11]
        cmp  r9b, '0'
        jb   .end
        cmp  r9b, '9'
        ja   .end
        cmp  r9b, 0
        je   .end
        imul rax, 10
        sub r9b, '0'
        add  rax, r9
        inc  r11
        jmp  .parse
    .end:
        mov rdx, r11
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .minus
    jmp parse_uint
    .minus:
        inc rdi
        call parse_uint
        neg rax
        test rdx, rdx
        jz .fail
        inc rdx
        ret
   .fail:
        xor rax, rax
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi
    cmp rdx, rax
    jb .fail
    xor r9, r9
    .copy:
        xor r10, r10
        mov r10b, byte[rdi+r9]
        mov byte[rsi+r9], r10b
        cmp r10, 0
        je .end
        inc r9
        jmp .copy
    .fail:
        xor rax, rax
        ret
    .end:
        mov rax, r9
        ret
